<%--
  Created by IntelliJ IDEA.
  User: yuan
  Date: 2020-06-25
  Time: 21:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
</head>
<body>

${username}<br>

用户名：<input type="text" name="pname" id="pname" /><br>
密码：<input type="password" name="password" id="password" /> <br>
年龄：<input type="text" name="page" id="page"><br>
<input type="button" value="测试" onclick="testJson()" />

<script>

    function testJson() {
        //获取输入的值pname为id
        alert($("#pname").val());
        var pname = $("#pname").val();
        var password = $("#password").val();
        var page = $("#page").val();
        console.log("${pageContext.request.contextPath}/user/send.do");
        var temp = JSON.stringify({
            "pname" : pname,
            password : password,
            page : page
        });
        console.log("json:"+temp);
        /* pname 可以加引号也可以不加
            "pname":pname
          json:{"pname":"11","password":"22","page":"33"}*/
        $.ajax({
            //请求路径
            url : "${pageContext.request.contextPath}/user/send.do",
            //请求类型
            type : "post",
            //data表示发送的数据
            data : JSON.stringify({
                pname : pname,
                password : password,
                page : page
            }), //定义发送请求的数据格式为JSON字符串
            contentType : "application/json;charset=utf-8",
            //定义回调响应的数据格式为JSON字符串，该属性可以省略
            dataType : "json",
            //成功响应的结果
            success : function(data) {
                if (data != null) {
                    alert("输入的用户名：" + data.pname + "，密码：" + data.password
                        + "， 年龄：" + data.page);
                }
            }
        });
    }

</script>

</body>
</html>
