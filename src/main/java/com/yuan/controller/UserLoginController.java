package com.yuan.controller;

import com.yuan.dao.UserMapper;
import com.yuan.entity.Person;
import com.yuan.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserLoginController {

    @Autowired
    private IUserService userService;
    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/login")
    public ModelAndView login(ModelAndView modelAndView){
        System.out.println("login");
        modelAndView = new ModelAndView("/user");
        modelAndView.addObject("username","yuan袁");
        return modelAndView;
    }

    @RequestMapping("send")
    @ResponseBody
    public Person receiveJson(@RequestBody Person person){
        //调用 service 的方法
        userService.findUser();
        System.out.println("-----------");
        System.out.println("receive json date");
        System.out.println(person);
        return person;
    }

}
