package com.yuan.dao;

import com.yuan.entity.User;

public interface UserMapper {

    public  User findUserById(int id);
}

