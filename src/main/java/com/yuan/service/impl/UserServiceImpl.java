package com.yuan.service.impl;

import com.yuan.dao.UserMapper;
import com.yuan.entity.User;
import com.yuan.service.IUserService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void findUser() {
        System.out.println("find user");
        User user = userMapper.findUserById(1);
        System.out.println(user);
        System.out.println(user.getUsername());
        System.out.println("findUser 方法结束，通过测试");
    }

}
