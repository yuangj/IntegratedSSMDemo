
# 参考文献
- [SSM 三大框架系列：Spring 5 + Spring MVC 5 + MyBatis 3.5 整合（附源码）](https://www.cnblogs.com/han-1034683568/p/12688791.html)
- [码云上的一个SSMDemo](https://gitee.com/devknight/ssmDemo.git)
- [ssm整合Mybatis无法连接数据库的问题 ‘Access denied for user...’](https://www.cnblogs.com/wangjr1994/p/12728573.html)
- [把本地项目上传到码云的整个过程（图文详解）](https://blog.csdn.net/qq_42943107/article/details/90739064)
# 说明
- 控制台乱码，在 tomcat 配置加上 -Dfile.encoding=UTF-8